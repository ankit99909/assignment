import React, { useEffect, useState } from 'react';
import { View, Text, Button,LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HomeScreen from './Index/Screen/Homescreen';
import SplashScreen from './Index/Screen/SplashScreen';
import IntroductionScreen from './Index/Screen/IntroductionScreen'; 
// Ignore log notification by message:
LogBox.ignoreLogs(['Warning: ...']);

// Ignore all log notifications:
LogBox.ignoreAllLogs();

const Stack = createNativeStackNavigator();

function App() {
  const [showIntroduction, setShowIntroduction] = useState(false);

  useEffect(() => {
    AsyncStorage.getItem('hasSeenIntroduction').then((value) => {
      if (value === null) {
        setShowIntroduction(true);
        AsyncStorage.setItem('hasSeenIntroduction', 'true');
      }
    });
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {showIntroduction ? (
          <Stack.Screen name="Introduction" component={IntroductionScreen} />
        ) : (
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
        )}
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

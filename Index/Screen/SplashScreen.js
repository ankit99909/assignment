import  {useEffect} from "react";
import { View,Text ,Image,StyleSheet} from "react-native"



const SplashScreen =({navigation})=>{
useEffect(()=>{
   setTimeout(() => {
    navigation.navigate("Home");
    
      }, 1000);
    },[])

    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image
        source={require('../../assets/logo.png')}
          style={{width:"70%", height: 60}}
        
        />
      </View> 
    )

}


export default SplashScreen ;
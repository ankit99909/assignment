import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const IntroductionScreen = () => {
  const navigation = useNavigation();

  const handleContinue = () => {
    // Navigate to the next screen, e.g., the main app screen
    navigation.navigate('Home');
  };

  return (
    <View style={styles.container}>
      {/* Your introduction screen content */}
      <Text style={styles.title}>Welcome to My App</Text>
      <Text style={styles.description}>This is an introduction to your app.</Text>
      
      <Button title="Continue" onPress={handleContinue} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  description: {
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 20,
  },
});

export default IntroductionScreen;

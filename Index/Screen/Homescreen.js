import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Modal, Button ,StatusBar} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../StyleSheet';
import Header from './Header'; 

const Homescreen = () => {
  const [selectedArray, setSelectedArray] = useState([]);
  const [showParentModal, setShowParentModal] = useState(false);
  const [showChildModal, setShowChildModal] = useState(false);

  useEffect(() => {
  
    AsyncStorage.setItem('selectedArray', JSON.stringify(selectedArray));
  }, [selectedArray]);


  useEffect(() => {
    const loadSelectedArray = async () => {
      try {
        const storedSelectedArray = await AsyncStorage.getItem('selectedArray');
        if (storedSelectedArray !== null) {
          setSelectedArray(JSON.parse(storedSelectedArray));
        }
      } catch (error) {
     
        console.error('Error loading selectedArray from AsyncStorage:', error);
      }
    };

    loadSelectedArray();
  }, []);

  const Parent_Array = [
    {
      Id: 1,
      Children_id: [1, 2, 3, 4],
      Name: 'Fruits',
    },
    {
      Id: 2,
      Children_id: [3, 4, 6],
      Name: 'Vegetables',
    },
  ];

  const Children_Array = [
    { id: 1, name: 'Apple' },
    { id: 2, name: 'Mango' },
    { id: 3, name: 'Banana' },
    { id: 4, name: 'Grapes' },
    { id: 6, name: 'Carrot' },
  ];

  const handleParentPress = (parentId) => {
    const parent = Parent_Array.find((item) => item.Id === parentId);
    const newSelectedArray = [...selectedArray];

  
    if (!newSelectedArray.includes(parent.Id)) {
      newSelectedArray.push(parent.Id);
    }

  
    parent.Children_id.forEach((childId) => {
      if (!newSelectedArray.includes(childId)) {
        newSelectedArray.push(childId);
      }
    });

    setSelectedArray(newSelectedArray);

    if (newSelectedArray.length % 2 === 0) {
      setShowParentModal(true);
    }
  };

  const handleChildPress = (childId) => {
    const newSelectedArray = [...selectedArray];

  
    if (!newSelectedArray.includes(childId)) {
      newSelectedArray.push(childId);
    } else {
      newSelectedArray.splice(newSelectedArray.indexOf(childId), 1);
    }

    setSelectedArray(newSelectedArray);


    if (
      newSelectedArray.filter((id) =>
        Children_Array.some((child) => child.id === id)
      ).length % 5 === 0
    ) {
      setShowChildModal(true);
    }
  };

  useEffect(() => {
  }, [selectedArray]);

  return (
    <View style={styles. mainContainer}>
       <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "lightblue" translucent = {true}/>
       <Header title="Home" />
    <View style={styles.container}>
     <Modal
  animationType="slide"
  transparent={true}
  visible={showParentModal}
>
  <View style={styles.modalContainer}>
    <Text style={styles.modalText}>
      Parent Modal - Show every 2 parent selections
    </Text>
    <Button title="Close" onPress={() => setShowParentModal(false)} />
  </View>
</Modal>

<Modal
  animationType="slide"
  transparent={true}
  visible={showChildModal}
>
  <View style={styles.modalContainer}>
    <Text style={styles.modalText}>
      Child Modal - Show every 5 child selections
    </Text>
    <Button title="Close" onPress={() => setShowChildModal(false)} />
  </View>
</Modal>

      {Parent_Array.map((parent) => (
        <View key={parent.Id} style={styles.parentContainer}>
          <TouchableOpacity onPress={() => handleParentPress(parent.Id)}>
            <Text style={styles.parentText}>{parent.Name}</Text>
          </TouchableOpacity>
          {parent.Children_id.map((childId) => {
            const child = Children_Array.find((c) => c.id === childId);
            if (child) {
              return (
                <TouchableOpacity
                  key={childId}
                  onPress={() => handleChildPress(childId)}
                  style={[
                    styles.childContainer,
                    selectedArray.includes(childId)
                      ? styles.selectedChild
                      : null,
                  ]}
                >
                  <Text style={styles.childText}>{child.name}</Text>
                </TouchableOpacity>
              );
            }
            return null;
          })}
        </View>
      ))}
    </View>
    </View>
  );
};

export default Homescreen;

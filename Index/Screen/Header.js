import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({ title }) => {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'lightblue',
    padding: 20,
    alignItems: 'flex-start', 
    justifyContent: 'flex-end',
    height: 100, 
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'YourCustomFont',
  },
});

export default Header;

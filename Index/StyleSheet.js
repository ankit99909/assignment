import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    // justifyContent: "center",
    // padding: 16,
  },
  container: {
    // flex: 1,
    backgroundColor: '#f0f0f0',
    // padding: 16,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20, 
    margin: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 5, 
  },
  modalText: {
    fontSize: 20, 
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  modalButton: {
    backgroundColor: '#007BFF',
    padding: 14, 
    borderRadius: 8,
    marginTop: 20,
  },
  modalButtonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  
  parentContainer: {
    backgroundColor: 'white', 
    borderRadius: 10,
    marginBottom: 16,
    padding: 16,
  },
  parentTouchable: {
    flex: 1,
  },
  parentText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  childContainer: {
    backgroundColor: '#EFEFEF', 
    borderRadius: 5,
    padding: 10,
    marginBottom: 8,
  },
  childText: {
    fontSize: 18,
  },
  modal: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    marginBottom: 16,
    textAlign: 'center',
  },
  modalButtonClose: {
    backgroundColor: '#DD2C00',
    padding: 12,
    borderRadius: 5,
  },
  modalButtonCloseText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
});
